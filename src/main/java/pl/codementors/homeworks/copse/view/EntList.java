package pl.codementors.homeworks.copse.view;

import com.sun.javafx.tk.Toolkit;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import pl.codementors.homeworks.copse.CopseMain;
import pl.codementors.homeworks.copse.model.Ent;
import pl.codementors.homeworks.copse.workers.OpenEnts;
import pl.codementors.homeworks.copse.workers.SaveEnts;


import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class EntList implements Initializable {

    @FXML
    private TableView entTable;

    @FXML
    private TableColumn<Ent, String> yearOfPlantingColumn;

    @FXML
    private TableColumn<Ent, Integer> heightColumn;

    @FXML
    private TableColumn<Ent, String> speciesColumn;

    @FXML
    private TableColumn<Ent, Ent.Type> typeColumn;

    @FXML
    private ProgressBar progress;

    private ObservableList<Ent> ents = FXCollections.observableArrayList();

    private ResourceBundle rb;

    public void initialize(URL url, ResourceBundle resourceBundle){
        setEnts();
        entTable.setItems(ents);
        rb = resourceBundle;

        yearOfPlantingColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        yearOfPlantingColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Ent, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Ent, String> event) {
                event.getRowValue().setYearOfPlanting(event.getNewValue());
            }
        });

        heightColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        heightColumn.setOnEditCommit(event -> event.getRowValue().setHeight(event.getNewValue()));

        speciesColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(Ent.entSpecies));
        speciesColumn.setOnEditCommit(event -> event.getRowValue().setSpecies(event.getNewValue()));

        typeColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(Ent.Type.values()));
        typeColumn.setOnEditCommit(event -> event.getRowValue().setType(event.getNewValue()));


        progress.setProgress(0);
    }

    private void setEnts(){
        ents.add(new Ent("1000", 9000, "Redwood", Ent.Type.CONIFEROUS));
        ents.add(new Ent("2018", 100, "Spruce", Ent.Type.CONIFEROUS));
        ents.add(new Ent("1200", 5000, "Oak", Ent.Type.LEAFY));
        ents.add(new Ent("1950", 2000, "Chestnut", Ent.Type.LEAFY));
        ents.add(new Ent("1900", 8000, "Beech", Ent.Type.LEAFY));
    }

    @FXML
    private void about(ActionEvent event){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(rb.getString("aboutTitle"));
        alert.setHeaderText(rb.getString("aboutHeader"));
        alert.setContentText(rb.getString("aboutContent"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();
    }

    @FXML
    private void open(ActionEvent event){
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(
                ((MenuItem)event.getTarget())
                .getParentPopup()
                .getScene()
                .getWindow());
        if(file != null){
            open(file);
        }
    }

    private void open(File file){
        OpenEnts openEnts = new OpenEnts(ents, file);
        progress.progressProperty().bind(openEnts.progressProperty());
        new Thread(openEnts).start();
    }

    @FXML
    private void save(ActionEvent event){
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(
                ((MenuItem)event.getTarget())
                .getParentPopup()
                .getScene()
                .getWindow());
        if(file != null){
            save(file);
        }
    }

    private void save(File file){
        SaveEnts saveEnts = new SaveEnts(ents, file);
        progress.progressProperty().bind(saveEnts.progressProperty());
        new Thread(saveEnts).start();
    }

    @FXML
    private void plant(ActionEvent event){
        ents.add(new Ent("2018", 0));
    }

    @FXML
    private void cut(ActionEvent event){
        if(entTable.getSelectionModel().getSelectedIndex() >= 0){
            ents.remove(entTable.getSelectionModel().getSelectedIndex());
            entTable.getSelectionModel().clearSelection();
        }
    }

    @FXML
    private void roottheseedling(ActionEvent event){
        if(entTable.getSelectionModel().getSelectedIndex() >= 0){
            Ent entToRoot = ents.get(entTable.getSelectionModel().getSelectedIndex());
            ents.add(new Ent("2018", 10, entToRoot.getSpecies(), entToRoot.getType()));
        }
    }
}
