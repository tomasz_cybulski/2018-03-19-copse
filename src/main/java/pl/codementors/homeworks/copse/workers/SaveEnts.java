package pl.codementors.homeworks.copse.workers;

import javafx.concurrent.Task;
import javafx.scene.control.ProgressBar;
import pl.codementors.homeworks.copse.model.Ent;

import java.io.*;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaveEnts extends Task<Void> {

    private static final Logger log = Logger.getLogger(OpenEnts.class.getCanonicalName());

    private Collection<Ent> ents;

    private File file;

    private ProgressBar progressBar;

    public SaveEnts(Collection<Ent> ents, File file){
        this.ents = ents;
        this.file = file;
    }

    @Override
    protected Void call() throws Exception {
        int i=0;
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(ents.size());
            for (Ent ent : ents) {
                oos.writeObject(ent);
                Thread.sleep(500);
                updateProgress(++i, ents.size());
            }
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }
}
