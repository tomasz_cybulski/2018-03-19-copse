package pl.codementors.homeworks.copse;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class CopseMain extends Application {

    public static void main(String[] args){
        launch(args);
    }

//    public static final String[] entSpecies = {"Oak", "Redwood", "Chestnut", "Spruce", "Beech"};

    public void start(Stage stage) throws Exception {

        URL fxml = this.getClass().getResource("/pl/codementors/homeworks/copse/view/ent_list.fxml");
        ResourceBundle rb = ResourceBundle.getBundle("pl.codementors.homeworks.copse.view.messages.ent_list_msg");

        Parent root = FXMLLoader.load(fxml, rb);

        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add("/pl/codementors/homeworks/copse/view/css/ent_list.css");

        stage.setScene(scene);
        stage.show();
    }
}
