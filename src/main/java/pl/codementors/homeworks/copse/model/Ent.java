package pl.codementors.homeworks.copse.model;

import java.io.Serializable;

public class Ent implements Serializable {

    public static final String[] entSpecies = {"Oak", "Redwood", "Chestnut", "Spruce", "Beech"};

    private String yearOfPlanting;

    private int height;

    private String species;

    private Type type;

    public enum Type {
        LEAFY, CONIFEROUS
    }

    public Ent() {
    }

    public Ent(String yearOfPlanting, int height){
        this.yearOfPlanting = yearOfPlanting;
        this.height = height;
    }

    public Ent(String yearOfPlanting, int height, String species, Type type) {
        this.yearOfPlanting = yearOfPlanting;
        this.height = height;
        this.species = species;
        this.type = type;
    }

    public String getYearOfPlanting() {
        return yearOfPlanting;
    }

    public void setYearOfPlanting(String yearOfPlanting) {
        this.yearOfPlanting = yearOfPlanting;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
